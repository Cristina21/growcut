package segmentation.model;

import java.awt.*;

/**
 * Created by Cristina on 3/30/2019.
 */

public class Cell {
    private Label label; // -1, 0, 1 (back, neutru, front)
    private double strength;
    private Color color;

    public Cell(segmentation.model.Label label, double strength, Color color) {
        this.label = label;
        this.strength = strength;
        this.color = color;
    }

    public Cell() {
    }

    public segmentation.model.Label getLabel() {
        return label;
    }

    public void setLabel(segmentation.model.Label label) {
        this.label = label;
    }

    public double getStrength() {
        return strength;
    }

    public void setStrength(double strength) {
        this.strength = strength;
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color color) {
        this.color = color;
    }
}
