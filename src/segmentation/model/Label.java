package segmentation.model;

/**
 * Created by Cristina on 4/1/2019.
 */

public enum Label {
    BACKGROUND,
    FOREGROUND,
    NEUTRAL
}
