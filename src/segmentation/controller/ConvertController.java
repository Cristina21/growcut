package segmentation.controller;

/**
 * Created by Cristina on 3/13/2019.
 */

import segmentation.model.Cell;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.*;
import java.util.List;
import javax.imageio.ImageIO;

public class ConvertController {
    private static final Color GREY = new Color(150, 150, 150);

    public List<BufferedImage> run(BufferedImage originalImage, BufferedImage bufferedImageAttack) {
        int width = originalImage.getWidth();
        int height = originalImage.getHeight();
        BufferedImage imageAttack = createImageAttack255(bufferedImageAttack);
        ArrayList<ArrayList<Cell>> cellMatrix = createCellMatrix(originalImage, imageAttack, width, height);
        growCut(cellMatrix, width, height);
        List<BufferedImage> resultImages = new ArrayList<>();
        BufferedImage resultImage_foreground = exportImageByLabel(cellMatrix, originalImage, width, height, segmentation.model.Label.FOREGROUND);
        BufferedImage resultImage_background = exportImageByLabel(cellMatrix, originalImage, width, height, segmentation.model.Label.BACKGROUND);
        BufferedImage resultImage_neutral = exportImageByLabel(cellMatrix, originalImage, width, height, segmentation.model.Label.NEUTRAL);
        resultImages.addAll(Arrays.asList(resultImage_foreground, resultImage_background, resultImage_neutral));
        return resultImages;
    }

    private BufferedImage exportImageByLabel(ArrayList<ArrayList<Cell>> cellMatrix, BufferedImage originalImage, int width, int height, segmentation.model.Label label) {
        BufferedImage resultImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        for (int y = 0; y < height; y++) { // linie
            ArrayList<Cell> row = cellMatrix.get(y);
            for (int x = 0; x < width; x++) { //coloana
                if(row.get(x).getLabel().equals(label)){
                    //adaug culoarea la imagine
                    resultImage.setRGB(x, y, originalImage.getRGB(x, y));
                }
            }
        }
        return resultImage;
    }

    private BufferedImage getImageFromFile(final String path) throws IOException {
        File base = new File(path);
        BufferedImage initImage = ImageIO.read(base);
        int width = initImage.getWidth(null);
        int height = initImage.getHeight(null);
        BufferedImage originalImage = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
        originalImage.getGraphics().drawImage(initImage, 0, 0, null);
        return originalImage;
    }

    /** TODO: incep parcurgerea de la celula marcata in front/back
     *
     * @param cellMatrix matrice de celule.
     * @param width dim.
     * @param height dim.
     */
    private void growCut(ArrayList<ArrayList<Cell>> cellMatrix, int width, int height) {
        //parcurgeme matrice de Celule
        for (int y = 0; y < height; y++) { // linie
            ArrayList<Cell> row = cellMatrix.get(y);
            for (int x = 0; x < width; x++) { //coloana
                // starea curenta (care va fi cea anterioara)
                Cell thisCell = row.get(x); // se ia de pe linia y, apoi de pe coloana x
                List<Cell> neighbours = getNeumannNeighbours(cellMatrix, y, x, width, height);
                for (Cell neighbour : neighbours) {
                    double attackStrength = attack(difference(thisCell.getColor(), neighbour.getColor())) * neighbour.getStrength();
                    if (attackStrength > thisCell.getStrength()) {
                        thisCell.setLabel(neighbour.getLabel());
                        thisCell.setStrength(attackStrength);
                    }
                }
            }
        }
    }

    private double attack(double difference) {
        double max2 = 255 * 255;
        double maxS = Math.sqrt(max2 + max2 + max2);
        return 1 - difference / maxS;
    }

    private double difference(Color thisCell, Color neighbour) {
        double redDiff2 = thisCell.getRed() - neighbour.getRed();
        redDiff2 = redDiff2 * redDiff2;
        double greenDiff2 = thisCell.getGreen() - neighbour.getGreen();
        greenDiff2 = greenDiff2 * greenDiff2;
        double blueDiff2 = thisCell.getBlue() - neighbour.getBlue();
        blueDiff2 = blueDiff2 * blueDiff2;
        return Math.sqrt(redDiff2 + greenDiff2 + blueDiff2);
    }

    private List<Cell> getNeumannNeighbours(ArrayList<ArrayList<Cell>> cellMatrix, int lineIndex, int columnIndex, int width, int height) {
        List<Cell> neighbours = new ArrayList<>();
        if (lineIndex == 0) {
            if (columnIndex == 0) {
                neighbours.add(cellMatrix.get(lineIndex).get(columnIndex + 1)); // dreapta
                neighbours.add(cellMatrix.get(lineIndex + 1).get(columnIndex)); // jos
            } else if (columnIndex == width - 1) {
                neighbours.add(cellMatrix.get(lineIndex).get(columnIndex - 1)); // stanga
                neighbours.add(cellMatrix.get(lineIndex + 1).get(columnIndex)); // jos
            } else {
                neighbours.add(cellMatrix.get(lineIndex).get(columnIndex - 1)); // stanga
                neighbours.add(cellMatrix.get(lineIndex).get(columnIndex + 1)); // dreapta
                neighbours.add(cellMatrix.get(lineIndex + 1).get(columnIndex)); // jos
            }
        } else if (lineIndex == height - 1) {
            if (columnIndex == 0) {
                neighbours.add(cellMatrix.get(lineIndex).get(columnIndex + 1)); // dreapta
                neighbours.add(cellMatrix.get(lineIndex - 1).get(columnIndex)); // sus
            } else if (columnIndex == width - 1) {
                neighbours.add(cellMatrix.get(lineIndex).get(columnIndex - 1)); // stanga
                neighbours.add(cellMatrix.get(lineIndex - 1).get(columnIndex)); // sus
            } else {
                neighbours.add(cellMatrix.get(lineIndex).get(columnIndex - 1)); // stanga
                neighbours.add(cellMatrix.get(lineIndex).get(columnIndex + 1)); // dreapta
                neighbours.add(cellMatrix.get(lineIndex - 1).get(columnIndex)); // sus
            }
        } else if (columnIndex == 0) {
            neighbours.add(cellMatrix.get(lineIndex).get(columnIndex + 1)); // dreapta
            neighbours.add(cellMatrix.get(lineIndex - 1).get(columnIndex)); // sus
            neighbours.add(cellMatrix.get(lineIndex + 1).get(columnIndex)); // jos
        } else if (columnIndex == width - 1) {
            neighbours.add(cellMatrix.get(lineIndex).get(columnIndex - 1)); // stanga
            neighbours.add(cellMatrix.get(lineIndex - 1).get(columnIndex)); // sus
            neighbours.add(cellMatrix.get(lineIndex + 1).get(columnIndex)); // jos
        } else { // e in mijloc
            neighbours.add(cellMatrix.get(lineIndex).get(columnIndex - 1)); // stanga
            neighbours.add(cellMatrix.get(lineIndex).get(columnIndex + 1)); // dreapta
            neighbours.add(cellMatrix.get(lineIndex - 1).get(columnIndex)); // sus
            neighbours.add(cellMatrix.get(lineIndex + 1).get(columnIndex)); // jos
        }
        return neighbours;
    }

    private BufferedImage createImageAttack255(BufferedImage image) {
        BufferedImage imageAttack = new BufferedImage(image.getWidth(), image.getHeight(), BufferedImage.TYPE_INT_ARGB);
        for (int x = 0; x < image.getWidth(); x++) {
            for (int y = 0; y < image.getHeight(); y++) {
                if(x > 1278 || y > 780){
                    int i = 1;
                }
                int pixel = image.getRGB(x, y);
                Color color = new Color(pixel);
                if (color.getRed() > 240 && color.getGreen() < 10 && color.getBlue() < 10) {
                    // 1 - foreground - white
                    imageAttack.setRGB(x, y, Color.RED.getRGB());
                } else if (color.getRed() < 10 && color.getGreen() < 10 && color.getBlue() > 240) {
                    // -1 - background - black
                    imageAttack.setRGB(x, y, Color.BLUE.getRGB());
                } else {
                    // 0 - neutral - gery
                    imageAttack.setRGB(x, y, GREY.getRGB());
                }
            }
        }
        return imageAttack;
    }

    private ArrayList<ArrayList<Cell>> createCellMatrix(BufferedImage image, BufferedImage imageMask, int width, int height) {
        ArrayList<ArrayList<Cell>> cellMatrix = new ArrayList<>();
        for (int y = 0; y < height; y++) {
            ArrayList<Cell> line = new ArrayList<>();
            for (int x = 0; x < width; x++) {
                int pixel = image.getRGB(x, y);
                int pixelMask = imageMask.getRGB(x, y);

                Color color = new Color(pixel);
                Color colorMask = new Color(pixelMask);

                int r = colorMask.getRed();
                int b = colorMask.getBlue();
                // 3 cases:
                // For strength: - 1, daca are eticheta (rosu/albastra) 0.5 daca e gri
                //for label // -1, 0, 1 (back - albastru, neutru - gri, front - rosu)
                Cell cell = new Cell();
                cell.setColor(color);
                if (colorMask.equals(Color.RED)) {
                    cell.setStrength(100);
                    cell.setLabel(segmentation.model.Label.FOREGROUND); // front
                } else if (colorMask.equals(Color.BLUE)) {
                    cell.setStrength(100);
                    cell.setLabel(segmentation.model.Label.BACKGROUND);
                } else {
                    cell.setStrength(0);
                    cell.setLabel(segmentation.model.Label.NEUTRAL);
                }
                line.add(cell);
            }
            cellMatrix.add(line);
        }
        return cellMatrix;
    }
}