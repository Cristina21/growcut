package segmentation.view;

import javafx.embed.swing.SwingFXUtils;
import javafx.geometry.Insets;
import javafx.scene.Cursor;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.scene.canvas.GraphicsContext;
import javafx.scene.control.*;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.image.Image;
import javafx.scene.image.WritableImage;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.shape.*;
import javafx.scene.shape.Rectangle;
import javafx.scene.shape.Shape;
import javafx.stage.FileChooser;
import javafx.stage.Stage;
import segmentation.controller.ConvertController;

import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.image.RenderedImage;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;
import java.util.Stack;

/**
 * Created by Cristina on 6/3/2019.
 * ===================================================
 * contents
 * ===================================================
 * 00- Free drawCanvas
 * 01- rubber
 * 02- drawCanvas Line
 * 03- drawCanvas Rectangele
 * 04- drawCanvas Circle
 * 05- drawCanvas Ellipse
 * ----------------------------------------------------
 * Features
 * ----------------------------------------------------
 * - the ability to change Line color
 * - the ability to change Fill color
 * - the ability to change Line width
 * - Undo & Redo
 * - Open Image && save Image
 * ____________________________________________________
 * problems
 * - undo & redo: not working with free drawCanvas and rubber
 * - Line & Rect & Circle: aren't be updated while drawing
 * ===================================================
 */

class Paint {
    private static final int WIDTH = 1200;
    private static final int HEIGHT = 700;
    private static final int WIDTH_CANVAS = 1000;
    private static final int HEIGHT_CANVAS = 650;

    private ConvertController convertController = new ConvertController();
    private BufferedImage originalImageCopy;

    private Canvas canvas = new Canvas(WIDTH_CANVAS, HEIGHT_CANVAS);
    private GraphicsContext graphicsContext;

    private Stack<Shape> undoHistory = new Stack();
    private Stack<Shape> redoHistory = new Stack();

    /* ---------- toggle buttons ---------- */
    private ToggleButton drawButton = new ToggleButton("Draw");
    private ToggleButton rubberButton = new ToggleButton("Rubber");
    private ToggleButton lineButton = new ToggleButton("Line");
    private ToggleButton rectangleButton = new ToggleButton("Rectange");
    private ToggleButton circleButton = new ToggleButton("Circle");
    private ToggleButton ellipseButton = new ToggleButton("Ellipse");

    /* ---------- buttons ---------- */
    private Button undoButton = new Button("Undo");
    private Button redoButton = new Button("Redo");
    private Button saveButton = new Button("Save");
    private Button openButton = new Button("Open");
    private Button runButton = new Button("Run");

    private Button selectRedColorButton = new Button("Foreground");
    private Button selectBlueColorButton = new Button("Background");
    private ColorPicker colorPickerLine = new ColorPicker(Color.BLACK);
    private ColorPicker colorPickerFill = new ColorPicker(Color.TRANSPARENT);

    private Slider slider = new Slider(1, 50, 3);
    private Label lineColorLabel = new Label("Line Color");
    private Label fillColorLabel = new Label("Fill Color");
    private Label lineWidthLabel = new Label("3.0");

    void start(Stage primaryStage) {
        setupToggleButtons();
        setupSlider();
        setupButtons();
        VBox buttons = getVBoxWithButtons();
        drawCanvas();
        colorPicker();
        slider(slider, lineWidthLabel);
        undo(undoButton);
        redo(redoButton);
        open(primaryStage, openButton);
        save(primaryStage, saveButton);
        run(runButton);

        /* ----------STAGE & SCENE---------- */
        ScrollPane pane = new ScrollPane();
        HBox hBoxContent = new HBox(buttons, canvas);
        pane.setContent(hBoxContent);

        Scene scene = new Scene(pane, WIDTH, HEIGHT);
        primaryStage.setTitle("Segmentare de imagini - GrowCut");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    /**
     * Setups the buttons.
     */
    private void setupButtons() {
        Button[] buttonsArray = {undoButton, redoButton, saveButton, openButton, runButton};
        for (Button button : buttonsArray) {
            button.setMinWidth(90);
            button.setCursor(Cursor.HAND);
            button.setTextFill(Color.WHITE);
            button.setStyle("-fx-background-color: #666;");
        }
        saveButton.setStyle("-fx-background-color: #80334d;");
        openButton.setStyle("-fx-background-color: #80334d;");
        runButton.setStyle("-fx-background-color: #80334d;");
    }

    /**
     * Creates a VBox object and organizes the buttons.
     *
     * @return a VBox object with the buttons in order.
     */
    private VBox getVBoxWithButtons() {
        VBox buttons = new VBox(10);
        buttons.getChildren().addAll(drawButton, rubberButton, lineButton, rectangleButton, circleButton, ellipseButton,
                lineColorLabel, selectRedColorButton, selectBlueColorButton, colorPickerLine, fillColorLabel, colorPickerFill,
                lineWidthLabel, slider, undoButton, redoButton, openButton, saveButton, runButton);
        buttons.setPadding(new Insets(5));
        buttons.setStyle("-fx-background-color: #999");
        buttons.setPrefWidth(100);
        return buttons;
    }

    /**
     * Setups the properties for slider.
     */
    private void setupSlider() {
        slider.setShowTickLabels(true);
        slider.setShowTickMarks(true);
    }

    /**
     * Setups the order and properties for the toggle buttons.
     */
    private void setupToggleButtons() {
        ToggleButton[] toggleButtonsArray = {drawButton, rubberButton, lineButton, rectangleButton, circleButton, ellipseButton};
        ToggleGroup tools = new ToggleGroup();
        for (ToggleButton toggleButton : toggleButtonsArray) {
            toggleButton.setMinWidth(90);
            toggleButton.setToggleGroup(tools);
            toggleButton.setCursor(Cursor.HAND);
        }
    }

    /**
     * Runs the growCut algorithm.
     *
     * @param runButton the run button.
     */
    private void run(Button runButton) {
        runButton.setOnAction((e) -> {
            WritableImage writableImage = new WritableImage(WIDTH, HEIGHT);
            canvas.snapshot(null, writableImage);
            RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
            BufferedImage bufferedImage = SwingFXUtils.fromFXImage(writableImage, null);
            List<BufferedImage> resultAll = convertController.run(originalImageCopy, bufferedImage);
            Image frontResult = SwingFXUtils.toFXImage(resultAll.get(0), null);
            graphicsContext.clearRect(0, 0, 1200, 1000);
            graphicsContext.drawImage(frontResult, 0, 0);
        });
    }

    /**
     * Saves the image locally.
     *
     * @param primaryStage the stage.
     * @param saveButton   the save button.
     */
    private void save(Stage primaryStage, Button saveButton) {
        saveButton.setOnAction((e) -> {
            FileChooser savefile = new FileChooser();
            savefile.setTitle("Save File");
            File file = savefile.showSaveDialog(primaryStage);
            if (file != null) {
                try {
                    WritableImage writableImage = new WritableImage(1080, 790);
                    canvas.snapshot(null, writableImage);
                    RenderedImage renderedImage = SwingFXUtils.fromFXImage(writableImage, null);
                    ImageIO.write(renderedImage, "png", file);
                } catch (IOException ex) {
                    System.out.println("Error on save!");
                }
            }
        });
    }

    /**
     * Opens an image from local memory.
     *
     * @param primaryStage the stage.
     * @param openButton   the open button.
     */
    private void open(Stage primaryStage, Button openButton) {
        openButton.setOnAction((e) -> {
            FileChooser openFile = new FileChooser();
            openFile.setTitle("Open File");
            File file = openFile.showOpenDialog(primaryStage);
            if (file != null) {
                try {
                    InputStream io = new FileInputStream(file);
                    Image image = new Image(io);
                    graphicsContext.drawImage(image, 0, 0);
                    originalImageCopy = SwingFXUtils.fromFXImage(image, null);
                } catch (IOException ex) {
                    System.out.println("Error on open!");
                }
            }
        });
    }

    /**
     * Redo the changes.
     *
     * @param redoButton the redo button.
     */
    private void redo(Button redoButton) {
        redoButton.setOnAction(e -> {
            if (!redoHistory.empty()) {
                Shape shape = redoHistory.lastElement();
                graphicsContext.setLineWidth(shape.getStrokeWidth());
                graphicsContext.setStroke(shape.getStroke());
                graphicsContext.setFill(shape.getFill());
                redoHistory.pop();
                if (shape.getClass() == Line.class) {
                    Line tempLine = (Line) shape;
                    graphicsContext.strokeLine(tempLine.getStartX(), tempLine.getStartY(), tempLine.getEndX(), tempLine.getEndY());
                    undoHistory.push(new Line(tempLine.getStartX(), tempLine.getStartY(), tempLine.getEndX(), tempLine.getEndY()));
                } else if (shape.getClass() == Rectangle.class) {
                    Rectangle tempRect = (Rectangle) shape;
                    graphicsContext.fillRect(tempRect.getX(), tempRect.getY(), tempRect.getWidth(), tempRect.getHeight());
                    graphicsContext.strokeRect(tempRect.getX(), tempRect.getY(), tempRect.getWidth(), tempRect.getHeight());

                    undoHistory.push(new Rectangle(tempRect.getX(), tempRect.getY(), tempRect.getWidth(), tempRect.getHeight()));
                } else if (shape.getClass() == Circle.class) {
                    Circle tempCirc = (Circle) shape;
                    graphicsContext.fillOval(tempCirc.getCenterX(), tempCirc.getCenterY(), tempCirc.getRadius(), tempCirc.getRadius());
                    graphicsContext.strokeOval(tempCirc.getCenterX(), tempCirc.getCenterY(), tempCirc.getRadius(), tempCirc.getRadius());

                    undoHistory.push(new Circle(tempCirc.getCenterX(), tempCirc.getCenterY(), tempCirc.getRadius()));
                } else if (shape.getClass() == Ellipse.class) {
                    Ellipse tempElps = (Ellipse) shape;
                    graphicsContext.fillOval(tempElps.getCenterX(), tempElps.getCenterY(), tempElps.getRadiusX(), tempElps.getRadiusY());
                    graphicsContext.strokeOval(tempElps.getCenterX(), tempElps.getCenterY(), tempElps.getRadiusX(), tempElps.getRadiusY());
                    undoHistory.push(new Ellipse(tempElps.getCenterX(), tempElps.getCenterY(), tempElps.getRadiusX(), tempElps.getRadiusY()));
                }
                Shape lastUndo = undoHistory.lastElement();
                lastUndo.setFill(graphicsContext.getFill());
                lastUndo.setStroke(graphicsContext.getStroke());
                lastUndo.setStrokeWidth(graphicsContext.getLineWidth());
            } else {
                System.out.println("there is no action to redo");
            }
        });
    }

    /**
     * Undo the changes.
     *
     * @param undoButton the undo button.
     */
    private void undo(Button undoButton) {
        undoButton.setOnAction(e -> {
            if (!undoHistory.empty()) {
                graphicsContext.clearRect(0, 0, 1080, 790);
                Shape removedShape = undoHistory.lastElement();
                if (removedShape.getClass() == Line.class) {
                    Line tempLine = (Line) removedShape;
                    tempLine.setFill(graphicsContext.getFill());
                    tempLine.setStroke(graphicsContext.getStroke());
                    tempLine.setStrokeWidth(graphicsContext.getLineWidth());
                    redoHistory.push(new Line(tempLine.getStartX(), tempLine.getStartY(), tempLine.getEndX(), tempLine.getEndY()));
                } else if (removedShape.getClass() == Rectangle.class) {
                    Rectangle tempRect = (Rectangle) removedShape;
                    tempRect.setFill(graphicsContext.getFill());
                    tempRect.setStroke(graphicsContext.getStroke());
                    tempRect.setStrokeWidth(graphicsContext.getLineWidth());
                    redoHistory.push(new Rectangle(tempRect.getX(), tempRect.getY(), tempRect.getWidth(), tempRect.getHeight()));
                } else if (removedShape.getClass() == Circle.class) {
                    Circle tempCirc = (Circle) removedShape;
                    tempCirc.setStrokeWidth(graphicsContext.getLineWidth());
                    tempCirc.setFill(graphicsContext.getFill());
                    tempCirc.setStroke(graphicsContext.getStroke());
                    redoHistory.push(new Circle(tempCirc.getCenterX(), tempCirc.getCenterY(), tempCirc.getRadius()));
                } else if (removedShape.getClass() == Ellipse.class) {
                    Ellipse tempElps = (Ellipse) removedShape;
                    tempElps.setFill(graphicsContext.getFill());
                    tempElps.setStroke(graphicsContext.getStroke());
                    tempElps.setStrokeWidth(graphicsContext.getLineWidth());
                    redoHistory.push(new Ellipse(tempElps.getCenterX(), tempElps.getCenterY(), tempElps.getRadiusX(), tempElps.getRadiusY()));
                }
                Shape lastRedo = redoHistory.lastElement();
                lastRedo.setFill(removedShape.getFill());
                lastRedo.setStroke(removedShape.getStroke());
                lastRedo.setStrokeWidth(removedShape.getStrokeWidth());
                undoHistory.pop();
                for (int i = 0; i < undoHistory.size(); i++) {
                    Shape shape = undoHistory.elementAt(i);
                    if (shape.getClass() == Line.class) {
                        Line temp = (Line) shape;
                        graphicsContext.setLineWidth(temp.getStrokeWidth());
                        graphicsContext.setStroke(temp.getStroke());
                        graphicsContext.setFill(temp.getFill());
                        graphicsContext.strokeLine(temp.getStartX(), temp.getStartY(), temp.getEndX(), temp.getEndY());
                    } else if (shape.getClass() == Rectangle.class) {
                        Rectangle temp = (Rectangle) shape;
                        graphicsContext.setLineWidth(temp.getStrokeWidth());
                        graphicsContext.setStroke(temp.getStroke());
                        graphicsContext.setFill(temp.getFill());
                        graphicsContext.fillRect(temp.getX(), temp.getY(), temp.getWidth(), temp.getHeight());
                        graphicsContext.strokeRect(temp.getX(), temp.getY(), temp.getWidth(), temp.getHeight());
                    } else if (shape.getClass() == Circle.class) {
                        Circle temp = (Circle) shape;
                        graphicsContext.setLineWidth(temp.getStrokeWidth());
                        graphicsContext.setStroke(temp.getStroke());
                        graphicsContext.setFill(temp.getFill());
                        graphicsContext.fillOval(temp.getCenterX(), temp.getCenterY(), temp.getRadius(), temp.getRadius());
                        graphicsContext.strokeOval(temp.getCenterX(), temp.getCenterY(), temp.getRadius(), temp.getRadius());
                    } else if (shape.getClass() == Ellipse.class) {
                        Ellipse temp = (Ellipse) shape;
                        graphicsContext.setLineWidth(temp.getStrokeWidth());
                        graphicsContext.setStroke(temp.getStroke());
                        graphicsContext.setFill(temp.getFill());
                        graphicsContext.fillOval(temp.getCenterX(), temp.getCenterY(), temp.getRadiusX(), temp.getRadiusY());
                        graphicsContext.strokeOval(temp.getCenterX(), temp.getCenterY(), temp.getRadiusX(), temp.getRadiusY());
                    }
                }
            } else {
                System.out.println("there is no action to undo");
            }
        });
    }

    /**
     * The slider sets the line widths.
     *
     * @param slider     the slider.
     * @param line_width the line width.
     */
    private void slider(Slider slider, Label line_width) {
        slider.valueProperty().addListener(e -> {
            double width = slider.getValue();
            line_width.setText(String.format("%.1f", width));
            graphicsContext.setLineWidth(width);
        });
    }

    /**
     * The color picker for the line and for filling the shapes.
     */
    private void colorPicker() {
        selectRedColorButton.setStyle("-fx-background-color: #ff0000;");
        selectBlueColorButton.setStyle("-fx-background-color: #0000ff;");
        selectRedColorButton.setOnAction(e -> {
            colorPickerLine.setValue(Color.RED);
            graphicsContext.setStroke(colorPickerLine.getValue());
        });
        selectBlueColorButton.setOnAction(e -> {
            colorPickerLine.setValue(Color.BLUE);
            graphicsContext.setStroke(colorPickerLine.getValue());
        });
        colorPickerLine.setOnAction(e -> {
            graphicsContext.setStroke(colorPickerLine.getValue());
        });
        colorPickerFill.setOnAction(e -> {
            graphicsContext.setFill(colorPickerFill.getValue());
        });
    }

    /**
     * Draw the canvas.
     */
    private void drawCanvas() {
        graphicsContext = canvas.getGraphicsContext2D();
        graphicsContext.setLineWidth(1);
        Line line = new Line();
        Rectangle rect = new Rectangle();
        Circle circ = new Circle();
        Ellipse elps = new Ellipse();
        canvas.setOnMousePressed(e -> {
            if (drawButton.isSelected()) {
                graphicsContext.setStroke(colorPickerLine.getValue());
                graphicsContext.beginPath();
                graphicsContext.lineTo(e.getX(), e.getY());
            } else if (rubberButton.isSelected()) {
                double lineWidth = graphicsContext.getLineWidth();
                graphicsContext.clearRect(e.getX() - lineWidth / 2, e.getY() - lineWidth / 2, lineWidth, lineWidth);
            } else if (lineButton.isSelected()) {
                graphicsContext.setStroke(colorPickerLine.getValue());
                line.setStartX(e.getX());
                line.setStartY(e.getY());
            } else if (rectangleButton.isSelected()) {
                graphicsContext.setStroke(colorPickerLine.getValue());
                graphicsContext.setFill(colorPickerFill.getValue());
                rect.setX(e.getX());
                rect.setY(e.getY());
            } else if (circleButton.isSelected()) {
                graphicsContext.setStroke(colorPickerLine.getValue());
                graphicsContext.setFill(colorPickerFill.getValue());
                circ.setCenterX(e.getX());
                circ.setCenterY(e.getY());
            } else if (ellipseButton.isSelected()) {
                graphicsContext.setStroke(colorPickerLine.getValue());
                graphicsContext.setFill(colorPickerFill.getValue());
                elps.setCenterX(e.getX());
                elps.setCenterY(e.getY());
            }
        });
        canvas.setOnMouseDragged(e -> {
            if (drawButton.isSelected()) {
                graphicsContext.lineTo(e.getX(), e.getY());
                graphicsContext.stroke();
            } else if (rubberButton.isSelected()) {
                double lineWidth = graphicsContext.getLineWidth();
                graphicsContext.clearRect(e.getX() - lineWidth / 2, e.getY() - lineWidth / 2, lineWidth, lineWidth);
            }
        });
        canvas.setOnMouseReleased(e -> {
            if (drawButton.isSelected()) {
                graphicsContext.lineTo(e.getX(), e.getY());
                graphicsContext.stroke();
                graphicsContext.closePath();
            } else if (rubberButton.isSelected()) {
                double lineWidth = graphicsContext.getLineWidth();
                graphicsContext.clearRect(e.getX() - lineWidth / 2, e.getY() - lineWidth / 2, lineWidth, lineWidth);
            } else if (lineButton.isSelected()) {
                line.setEndX(e.getX());
                line.setEndY(e.getY());
                graphicsContext.strokeLine(line.getStartX(), line.getStartY(), line.getEndX(), line.getEndY());
                undoHistory.push(new Line(line.getStartX(), line.getStartY(), line.getEndX(), line.getEndY()));
            } else if (rectangleButton.isSelected()) {
                rect.setWidth(Math.abs((e.getX() - rect.getX())));
                rect.setHeight(Math.abs((e.getY() - rect.getY())));
                //rect.setX((rect.getX() > e.getX()) ? e.getX(): rect.getX());
                if (rect.getX() > e.getX()) {
                    rect.setX(e.getX());
                }
                //rect.setY((rect.getY() > e.getY()) ? e.getY(): rect.getY());
                if (rect.getY() > e.getY()) {
                    rect.setY(e.getY());
                }

                graphicsContext.fillRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
                graphicsContext.strokeRect(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight());
                undoHistory.push(new Rectangle(rect.getX(), rect.getY(), rect.getWidth(), rect.getHeight()));
            } else if (circleButton.isSelected()) {
                circ.setRadius((Math.abs(e.getX() - circ.getCenterX()) + Math.abs(e.getY() - circ.getCenterY())) / 2);
                if (circ.getCenterX() > e.getX()) {
                    circ.setCenterX(e.getX());
                }
                if (circ.getCenterY() > e.getY()) {
                    circ.setCenterY(e.getY());
                }
                graphicsContext.fillOval(circ.getCenterX(), circ.getCenterY(), circ.getRadius(), circ.getRadius());
                graphicsContext.strokeOval(circ.getCenterX(), circ.getCenterY(), circ.getRadius(), circ.getRadius());
                undoHistory.push(new Circle(circ.getCenterX(), circ.getCenterY(), circ.getRadius()));
            } else if (ellipseButton.isSelected()) {
                elps.setRadiusX(Math.abs(e.getX() - elps.getCenterX()));
                elps.setRadiusY(Math.abs(e.getY() - elps.getCenterY()));
                if (elps.getCenterX() > e.getX()) {
                    elps.setCenterX(e.getX());
                }
                if (elps.getCenterY() > e.getY()) {
                    elps.setCenterY(e.getY());
                }
                graphicsContext.strokeOval(elps.getCenterX(), elps.getCenterY(), elps.getRadiusX(), elps.getRadiusY());
                graphicsContext.fillOval(elps.getCenterX(), elps.getCenterY(), elps.getRadiusX(), elps.getRadiusY());
                undoHistory.push(new Ellipse(elps.getCenterX(), elps.getCenterY(), elps.getRadiusX(), elps.getRadiusY()));
            }
            redoHistory.clear();
            Shape lastUndo = undoHistory.lastElement();
            lastUndo.setFill(graphicsContext.getFill());
            lastUndo.setStroke(graphicsContext.getStroke());
            lastUndo.setStrokeWidth(graphicsContext.getLineWidth());

        });
    }
}
