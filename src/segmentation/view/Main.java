package segmentation.view;

/**
 * Created by Cristina on 4/4/2019.
 */

import javafx.application.Application;
import javafx.stage.Stage;

public class Main extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage primaryStage) {
        new Paint().start(primaryStage);
    }
}